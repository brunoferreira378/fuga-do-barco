﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Tubarao_script : MonoBehaviour {

    public static int vidaPersonagem;
    public int vidaTotal = 100;
    public static int pontuacao = 0;
    public Texture sangue;
    public bool esquerda = false;
    public bool cima = false;
    public bool baixo = false;
    public float velocidade;
    Animator anim;

    void Start ()
    {
       vidaPersonagem = vidaTotal;
       anim = GetComponent<Animator>();
	}

    void OnGUI()
    {
        GUI.DrawTexture(new Rect(Screen.width / 25, Screen.height / 15, Screen.width / 5.5f / vidaTotal * vidaPersonagem, Screen.height / 25), sangue);
    }

    public static void PerderVida()
    {       
        vidaPersonagem -= 10;
    }


    void Update ()
    {
        anim.SetInteger("estado", 0);

        if (Input.GetKey(KeyCode.A))
        {
            if(!esquerda)
            {
                transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z * -1);
                esquerda = true;
            }

            transform.Translate(0, 0, -velocidade);
            anim.SetInteger("estado", 1);
        }

        if (Input.GetKey(KeyCode.D))
        {
            if(esquerda)
            {
                transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z * -1);
                esquerda = false;
            }

            transform.Translate(0, 0, velocidade);
            anim.SetInteger("estado", 1);
        }

        if (Input.GetKey(KeyCode.W))
        {
            if(!cima)
            {
                transform.Rotate(Vector3.up * Time.deltaTime);
                cima = true;
                baixo = false;
            }
            transform.Translate(0, velocidade, 0);
            anim.SetInteger("estado", 2);
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(0, -velocidade, 0);
            anim.SetInteger("estado", 2);
        }

        if(vidaPersonagem == 10)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

    }
}
