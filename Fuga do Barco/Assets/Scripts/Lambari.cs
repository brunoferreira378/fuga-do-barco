﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lambari : MonoBehaviour
{
    public bool virado = false;
    public float velocidade;
    Rigidbody rigid;
    Animation animation;
    public AudioSource audio;
    public AudioClip sonar;

    void Start()
    {
        velocidade = -4;
        rigid = GetComponent<Rigidbody>();
        animation = GetComponent<Animation>();
        audio = GetComponent<AudioSource>();
        StartCoroutine(Destruir());
        transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z * -1);
    }

    // Update is called once per frame
    void Update()
    {
        if (virado)
        {
            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z * -1);
            virado = false;
        }  

        Nadar(velocidade);
        animation.Play("Nadando_lateral");
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "tubarao")
        {
            virado = true;
            velocidade = -(velocidade);
            audio.Play();
        }

        if(other.tag == "armadilha")
        {
            Destroy(this.gameObject);
            Tubarao_script.PerderVida();
        }

        if(other.tag == "pontuacao")
        {
            Destroy(this.gameObject);
            Pontuacao.Pontuar();           
        }
    }

    public IEnumerator Destruir()
    {
        yield return new WaitForSeconds(9f);
        Destroy(this.gameObject);
    }

    public void Nadar(float vel)
    {
        rigid.transform.Translate(0, 0, vel);
    }


}
