﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnLambari : MonoBehaviour {

    public GameObject spawner;
    public bool pararSpawnar = false;
    public float tempoSpawn;
    public float RecargaSpawn;

	void Start ()
    {
        tempoSpawn = Random.Range(1, 10);
        RecargaSpawn = Random.Range(1, 10);
        InvokeRepeating("SpawnarObjeto", tempoSpawn, RecargaSpawn);
	}
	
	public void SpawnarObjeto ()
    {
        Instantiate(spawner, transform.position, transform.rotation);
        if(pararSpawnar)
        {
            CancelInvoke("SpawnarObjeto");
        }
	}

}
