﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pontuacao : MonoBehaviour {

    public static Text textoPontuacao;

    void Start()
    {
        textoPontuacao = GetComponent<Text>();
    }
        public static void Pontuar()
    {
        Tubarao_script.pontuacao += 1;
        textoPontuacao.text = Tubarao_script.pontuacao.ToString();
    }
}
