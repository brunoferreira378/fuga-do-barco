﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agua : MonoBehaviour {

    Renderer rend;
    // Use this for initialization
    void Start ()
    {
        rend = GetComponent<Renderer>();
        DeixarTransparente();
    }

    void Update()
    {
        DeixarTransparente();
    }

    public void DeixarTransparente()
    {
        for (int m = 0; m < rend.materials.Length; m++)
        {
            if (rend.materials[m].color.a < 1)
            {
                rend.materials[m].shader = Shader.Find("Transparent/Diffuse");               
            }

            else
            {
                rend.materials[m].shader = Shader.Find("Diffuse");
            }

            if (rend.materials[m].color.a > 0.5f)
            {
                Color cor = rend.materials[m].color;
                cor.a -= 0.02f;
                rend.materials[m].color = cor;
            } 

        }
    }

}
